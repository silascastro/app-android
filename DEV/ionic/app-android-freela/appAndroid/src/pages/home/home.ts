import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  valor;
  quantidade;
  erroValor: boolean = false;
  erroValorMessage: string;
  erroQtd: boolean = false;
  erroQtdMessage: string;
  valorTotal:number;
  valorDesconto;
  valorFinal;
  constructor(public navCtrl: NavController) {

  }

  submit(){
    if(this.valor && this.quantidade){
      if(this.erroValor || this.erroQtd){
        if(this.erroQtd){
          this.erroQtd = false;
          this.erroQtdMessage = "";
        }
        if(this.erroValor){
          this.erroValor = false;
          this.erroValorMessage = "";
        }
      }
      var aux = this.valor.replace(",", ".");
      this.valorTotal = parseFloat(aux)*this.quantidade;
      this.valorDesconto = (this.valorTotal*0.10).toFixed(2);
      this.valorFinal = this.valorTotal - this.valorDesconto;

      console.log(this.valorTotal);
    }
    else{
      if(!this.valor){
        this.erroValor = true;
        this.erroValorMessage = 'digite um valor';
        if(this.quantidade && this.erroQtd){
          this.erroQtd = false;
          this.erroQtdMessage = "";
        }
      }
      if(!this.quantidade){
        this.erroQtd = true;
        this.erroQtdMessage = 'digite a quantidade';
        if(this.valor && this.erroValor){
          this.erroValor = false;
          this.erroValorMessage = "";
        }
      }
    }
  }

}
